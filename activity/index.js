"use strict";

const getCube = function(number){
    let cubeAnswer = number ** 3
    return(`The Cube of ${number} is ${cubeAnswer}`)
}

console.log(getCube(3))

const address = [38,2100]
const [addressNumber,zipCode] = address

console.log(`I live at ${addressNumber}B Gonzales Street, Pto. Rivas Itaas, Balanga City, Bataan ${zipCode} Zip code `)

const cat = {
    name : "ming-ming",
    color : "blue",
    age : 2,
}
const {name,color,age} = cat

console.log(`My cat's name is ${name}, his fur color is ${color} and he is ${age} years old`)

const array = [1,2,3,4,5]

array.forEach( e => console.log(e))

const arrayTwo = [2,3,10,3,7]

const reduceNumbers = arrayTwo.reduce((acc,num) => acc + num)

console.log(reduceNumbers)

class dog {
    constructor(name,age,breed) {
        this.name = name
        this.age = age
        this.breed = breed
    }
}

const buddy = new dog("Buddy", 4, "Golden Retriever")
const luna = new dog("Luna", 2, "German Shepherd")
const max = new dog("Max", 6, "Labrador Retriever")

console.log(buddy)
console.log(luna)
console.log(max)